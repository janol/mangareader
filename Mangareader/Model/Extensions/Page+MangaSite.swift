//
//  Page+MangaSite.swift
//  Mangareader
//
//  Created by Jan Olbrich on 04.01.16.
//  Copyright © 2016 Jan Olbrich. All rights reserved.
//

import Foundation
import JSONCodable

extension Page: JSONDecodable {
    init(object: JSONObject) throws {
        let decoder = JSONDecoder(object: object)
        url = try decoder.decode(MangaSiteAPI.JSONPageURL, transformer:JSONTransformers.PagesImageStringToNSURL)
    }
}