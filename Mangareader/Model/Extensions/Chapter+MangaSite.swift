//
//  Chapter+MangaSite.swift
//  Mangareader
//
//  Created by Jan Olbrich on 02.01.16.
//  Copyright © 2016 Jan Olbrich. All rights reserved.
//

import Foundation
import JSONCodable

extension Chapter: JSONDecodable {
    init(object: JSONObject) throws {
        let decoder = JSONDecoder(object: object)
        
        id = try decoder.decode(MangaSiteAPI.JSONChapterID)
        name = try decoder.decode(MangaSiteAPI.JSONChapterName)
        chapterNumber = try decoder.decode(MangaSiteAPI.JSONChapterNumber)
        pages = []
    }
    
     mutating func addPages(pagesList: JSONObject) throws {
        pages = try [Page](jsonPageDict: pagesList)
    }
}