//
//  Manga+Merge.swift
//  Mangareader
//
//  Created by Jan Olbrich on 13.01.16.
//  Copyright © 2016 Jan Olbrich. All rights reserved.
//

import Foundation

extension Manga {
    mutating func merge(manga: Manga) -> Manga {
        if self != manga {
            return self
        }
        
        if let cover = manga.coverImage {
            self.coverImage = cover
        }
        
        if let author = manga.author {
            self.author = author
        }
        
        if let description = manga.description {
            self.description = description
        }
        
        if let like = manga.like {
            self.like = like
        }
        
        if let favorite = manga.favorite {
            self.favorite = favorite
        }
        
        if let status = manga.status {
            self.status = status
        }
        
        if let chapters = manga.chapters {
            if chapters.count > 0 {
                self.chapters = chapters
            }
        }
        
        return self
    }
}