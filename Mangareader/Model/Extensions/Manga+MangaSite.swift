//
//  Manga+MangaSite.swift
//  Mangareader
//
//  Created by Jan Olbrich on 02.01.16.
//  Copyright © 2016 Jan Olbrich. All rights reserved.
//

import Foundation
import JSONCodable

extension Manga: JSONDecodable {
    init(object: JSONObject) throws {
        let decoder = JSONDecoder(object: object)
        id = try decoder.decode(MangaSiteAPI.JSONMangaID)
        name = try decoder.decode(MangaSiteAPI.JSONMangaName)
        coverImage = try decoder.decode(MangaSiteAPI.JSONMangaImage, transformer: JSONTransformers.CoverImageStringToNSURL)
        author = try decoder.decode(MangaSiteAPI.JSONMangaAuthor)
        description = try decoder.decode(MangaSiteAPI.JSONMangaDescription)
        like = try decoder.decode(MangaSiteAPI.JSONMangaLike, transformer: JSONTransformers.StringToBool)
        favorite = try decoder.decode(MangaSiteAPI.JSONMangaFavorite, transformer: JSONTransformers.StringToBool)
        status = try decoder.decode(MangaSiteAPI.JSONMangaStatus)
        chapters = []
    }
    
    mutating func addChapters(chapterList: JSONObject) throws {
        chapters = try [Chapter](jsonChapterDict: chapterList)
    }
}