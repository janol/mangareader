//
//  Array+MangaSite.swift
//  Mangareader
//
//  Created by Jan Olbrich on 18.01.16.
//  Copyright © 2016 Jan Olbrich. All rights reserved.
//

import Foundation
import JSONCodable


extension Array where Element : JSONDecodable {
    public init(jsonMangaDict: JSONObject) throws {
        let jsonList = jsonMangaDict[MangaSiteAPI.JSONMangaRoot] as! [AnyObject]
        try self.init(JSONArray: jsonList)
    }
    
    public init(jsonChapterDict: JSONObject) throws {
        let jsonList = jsonChapterDict[MangaSiteAPI.JSONChapterRoot] as! [AnyObject]
        try self.init(JSONArray: jsonList)
    }
    
    public init(jsonPageDict: JSONObject) throws {
        let jsonList = jsonPageDict[MangaSiteAPI.JSONPageRoot] as! [AnyObject]
        try self.init(JSONArray: jsonList)
    }
}
