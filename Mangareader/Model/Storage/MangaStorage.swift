//
//  MangaStorage.swift
//  Mangareader
//
//  Created by Jan Olbrich on 04.01.16.
//  Copyright © 2016 Jan Olbrich. All rights reserved.
//

import Foundation
import ReactiveCocoa
import JSONCodable

struct MangaStorage {
    private let client = MangaClient()
    private let clientID = "abcdefg" //this will be later replaced with a public API
    
    mutating func allMangaSignal() -> SignalProducer<[Manga], NSError> {
        return self.client.mangaList().flatMap(.Merge) { (mangaJSON) -> SignalProducer<[Manga], NSError> in
            do {
                let mangas = try [Manga](jsonMangaDict: mangaJSON)
                return SignalProducer(value: mangas)
            }
            catch {
                let error = NSError(domain: "JSON decoding error", code: 0, userInfo: nil)
                return SignalProducer(error: error)
            }
        }.producer
    }
    
    mutating func mangaChaptersSignal(var manga: Manga) -> SignalProducer<Manga, NSError> {
        return self.client.chaptersForMangaWithID(manga.id).flatMap(.Merge) { (chapterJSON) -> SignalProducer<Manga, NSError> in
            do {
                try manga.addChapters(chapterJSON)
                return SignalProducer(value: manga)
            }
            catch {
                let error = NSError(domain: "JSON decoding error", code: 0, userInfo: nil)
                return SignalProducer(error: error)
            }
        }.producer
    }
    
    mutating func mangaDetailsSignal(var manga: Manga) -> SignalProducer<Manga, NSError> {
        return self.client.detailsForMangaWithID(manga.id, clientID: self.clientID).flatMap(.Merge) { (mangaJSON) -> SignalProducer<Manga, NSError> in
            do {
                let mangas = try [Manga](jsonMangaDict: mangaJSON)
                manga.merge(mangas[0])
                return SignalProducer(value: manga)
            }
            catch {
                let error = NSError(domain: "JSON decoding error", code: 0, userInfo: nil)
                return SignalProducer(error: error)
            }
        }.producer
    }
    
    mutating func chapterPagesSignal(var chapter: Chapter) -> SignalProducer<Chapter, NSError> {
        return self.client.pagesForChapterWithID(chapter.id).flatMap(.Merge) { (pagesJSON) -> SignalProducer<Chapter, NSError> in
            do {
                try chapter.addPages(pagesJSON)
                return SignalProducer(value: chapter)
            }
            catch {
                let error = NSError(domain: "JSON decoding error", code: 0, userInfo: nil)
                return SignalProducer(error: error)
            }
        }.producer
    }
}