//
//  Manga.swift
//  Mangareader
//
//  Created by Jan Olbrich on 01.01.16.
//  Copyright © 2016 Jan Olbrich. All rights reserved.
//

import Foundation

struct Manga: Equatable {
    let id: String
    let name: String
    var coverImage: NSURL?
    var author: String?
    var description: String?
    var like: Bool?
    var favorite: Bool?
    var status: String?
    var chapters: [Chapter]?
}

func ==(lhs: Manga, rhs: Manga) -> Bool {
    return lhs.id == rhs.id
}