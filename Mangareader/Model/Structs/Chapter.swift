//
//  Chapter.swift
//  Mangareader
//
//  Created by Jan Olbrich on 02.01.16.
//  Copyright © 2016 Jan Olbrich. All rights reserved.
//

import Foundation

struct Chapter {
    let id: String
    let name: String
    let chapterNumber: String
    var pages: [Page]?
}