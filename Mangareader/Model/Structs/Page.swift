//
//  Page.swift
//  Mangareader
//
//  Created by Jan Olbrich on 04.01.16.
//  Copyright © 2016 Jan Olbrich. All rights reserved.
//

import Foundation

struct Page {
    let url: NSURL
}