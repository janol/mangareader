//
//  MangaClient.swift
//  Mangareader
//
//  Created by Jan Olbrich on 02.01.16.
//  Copyright © 2016 Jan Olbrich. All rights reserved.
//

import Foundation
import Alamofire
import ReactiveCocoa

struct MangaClient {
    
    func requestWithMethod(method: Alamofire.Method, address: String, parameters: Dictionary<String, String>) -> SignalProducer<Dictionary<String, AnyObject>, NSError> {
        return SignalProducer() { observer, disposable in
            Alamofire.request(method, address, parameters: parameters).responseJSON { response in
                guard let JSON = response.result.value else {
                    observer.sendFailed(response.result.error!)
                    return
                }
                
                observer.sendNext(JSON as! Dictionary<String, AnyObject>)
                observer.sendCompleted()
            }
        }.observeOn(QueueScheduler(qos: QOS_CLASS_USER_INITIATED, name: "NetworkRequestQueue"))
    }
    
}