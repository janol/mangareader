//
//  MangaClient+MangaSiteAPI.swift
//  Mangareader
//
//  Created by Jan Olbrich on 03.01.16.
//  Copyright © 2016 Jan Olbrich. All rights reserved.
//

import Foundation
import ReactiveCocoa

extension MangaClient {
    
    func mangaList() -> SignalProducer<Dictionary<String, AnyObject>, NSError> {
        return requestWithMethod(.GET, address: MangaSiteAPI.MangaListURL, parameters: [String:String]())
    }
    
    func detailsForMangaWithID(mangaId: String, clientID: String) -> SignalProducer<Dictionary<String, AnyObject>, NSError> {
        return requestWithMethod(.GET, address: MangaSiteAPI.MangaDetailsURL, parameters: [MangaSiteAPI.MangaID: mangaId, MangaSiteAPI.ClientID: clientID])
    }
    
    func chaptersForMangaWithID(mangaID: String) -> SignalProducer<Dictionary<String, AnyObject>, NSError> {
        return requestWithMethod(.GET, address: MangaSiteAPI.MangaChapterListURL, parameters: [MangaSiteAPI.MangaID: mangaID])
    }
    
    func favoritesForClientID(clientID: String) -> SignalProducer<Dictionary<String, AnyObject>, NSError> {
        return requestWithMethod(.GET, address: MangaSiteAPI.UserFavoritesURL, parameters: [MangaSiteAPI.ClientID: clientID])
    }
    
    func pagesForChapterWithID(chapterID: String) -> SignalProducer<Dictionary<String, AnyObject>, NSError> {
        return requestWithMethod(.GET, address: MangaSiteAPI.ChapterPagesListURL, parameters: [MangaSiteAPI.ChapterID: chapterID])
    }
}