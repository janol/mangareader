//
//  MangaTableViewCellAnimator.swift
//  Mangareader
//
//  Created by Jan Olbrich on 18.01.16.
//  Copyright © 2016 Jan Olbrich. All rights reserved.
//

import UIKit
import EasyAnimation

class MangaTableViewCellAnimator: NSObject, UIViewControllerAnimatedTransitioning {
    
    func transitionDuration(transitionContext: UIViewControllerContextTransitioning?) -> NSTimeInterval {
        return 1.0
    }
    
    func animateTransition(transitionContext: UIViewControllerContextTransitioning) {
        
        let fromVC = transitionContext.viewControllerForKey(UITransitionContextFromViewControllerKey) as! MangaListViewController
        let toVC = transitionContext.viewControllerForKey(UITransitionContextToViewControllerKey) as! MangaDetailViewController
        
        transitionContext.containerView()!.insertSubview(toVC.view, belowSubview: fromVC.view)
        
        //make a snapshot of the selected cell
        let cellView = fromVC.selectedCellSnapshot()
        transitionContext.containerView()!.addSubview(cellView)
        
        
        let width = fromVC.view.bounds.size.width
        toVC.view.transform = CGAffineTransformMakeTranslation(width, 0.0)
        
        (UIApplication.sharedApplication().windows.first!).backgroundColor = UIColor.whiteColor()
        
        let duration = transitionDuration(transitionContext)
        
        UIView.animateAndChainWithDuration(duration/4, delay: 0.0, options: [], animations: {
            fromVC.view.transform = CGAffineTransformMakeTranslation(-width, 0.0)
        }, completion:nil).animateWithDuration(duration/4, animations: {
            cellView.center.y = cellView.bounds.size.height/2 + fromVC.topLayoutGuide.length
        }).animateWithDuration(duration/4, animations: {
            toVC.view.transform = CGAffineTransformIdentity
        }).animateWithDuration(duration/4, animations: {
            cellView.alpha = 0.0
        }, completion: { _ in
            fromVC.view.transform = CGAffineTransformIdentity
            transitionContext.completeTransition(!transitionContext.transitionWasCancelled())
        })
        
    }
    
}