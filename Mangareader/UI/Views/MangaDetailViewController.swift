//
//  MangaDetailViewController.swift
//  Mangareader
//
//  Created by Jan Olbrich on 13.01.16.
//  Copyright © 2016 Jan Olbrich. All rights reserved.
//

import UIKit
import ReactiveCocoa
import AlamofireImage

class MangaDetailViewController: UIViewController, UITableViewDelegate, UITableViewDataSource {
    
    var viewModel: MangaDetailViewModel!
    
    @IBOutlet weak var titleLabel: UILabel!
    @IBOutlet weak var authorLabel: UILabel!
    @IBOutlet weak var descriptionLabel: UITextView!
    @IBOutlet weak var coverImageView: UIImageView!
    @IBOutlet weak var chaptersTableView: UITableView!
    
    override func viewWillAppear(animated: Bool) {
        super.viewWillAppear(animated)
        navigationController?.delegate = nil
        
        chaptersTableView.registerNib(UINib(nibName: "ChapterTableViewCell", bundle: nil), forCellReuseIdentifier: "ChapterCell")
        
        setupBindings()
    }
    
    func setupBindings() {
        self.titleLabel.rac_text <~ viewModel.title
        self.authorLabel.rac_text <~ viewModel.author
        self.descriptionLabel.rac_text <~ viewModel.mangaDescription
        
        viewModel.coverURL.producer.startWithNext { url in
            guard let _ = url.host else {
                return
            }
            
            self.coverImageView.af_setImageWithURL(url)
        }
        
        viewModel.chapters.producer.observeOn(UIScheduler()).startWithNext { _ in
            self.chaptersTableView.reloadData()
        }
    }
    
    func numberOfSectionsInTableView(tableView: UITableView) -> Int {
        return 1
    }
    
    func tableView(tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return viewModel.chapters.value.count
    }
    
    func tableView(tableView: UITableView, cellForRowAtIndexPath indexPath: NSIndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCellWithIdentifier("ChapterCell", forIndexPath: indexPath) as! ChapterTableViewCell
        
        cell.configure(viewModel.chapters.value[indexPath.row])
        
        return cell
    }
    
    func tableView(tableView: UITableView, didSelectRowAtIndexPath indexPath: NSIndexPath) {
        performSegueWithIdentifier("PageReader", sender: indexPath)
    }
    
    // MARK: - Navigation
    
    override func prepareForSegue(segue: UIStoryboardSegue, sender: AnyObject?) {
        let controller = segue.destinationViewController as! PageReaderViewController
        controller.viewModel = self.viewModel.pagesReaderViewModel(sender as! NSIndexPath)
    }
}
