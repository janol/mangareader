//
//  MangaListViewController.swift
//  Mangareader
//
//  Created by Jan Olbrich on 07.01.16.
//  Copyright © 2016 Jan Olbrich. All rights reserved.
//

import UIKit
import ReactiveCocoa

class MangaListViewController: UITableViewController {
    
    var viewModel: MangaListViewModel!
    
    var lastSelectedIndexPath: NSIndexPath?
    let transition = MangaTableViewCellAnimator()
    var messageLabel: UILabel!
    var refreshAction: CocoaAction!     // Needed to have a strong reference.. Otherwise it will be removed

    override func viewDidLoad() {
        super.viewDidLoad()

        viewModel = MangaListViewModel()
        
        tableView.registerNib(UINib(nibName: "MangaTableViewCell", bundle: nil), forCellReuseIdentifier: "MangaCell")

        refreshControl = UIRefreshControl()
        
        addBackgroundLabel()
        
        setupBindings()
        
    }
    
    override func viewWillAppear(animated: Bool) {
        super.viewWillAppear(animated)
        
        navigationController?.delegate = self
    }
    
    func setupBindings() {
        viewModel.mangas.producer.observeOn(UIScheduler()).startWithNext() { _ in
            self.tableView.reloadData()
        }
        
        refreshAction = CocoaAction(viewModel.updateAction, input: "")
        refreshControl?.addTarget(refreshAction, action: CocoaAction.selector, forControlEvents: .ValueChanged)
        viewModel.updateAction.events.dematerialize().observeOn(UIScheduler()).observeNext { _ in
            self.refreshControl?.endRefreshing()
        }
        
        viewModel.tableViewSeparatorHidden.producer.startWithNext { hidden in
            self.tableView.separatorStyle = hidden ? .None : .SingleLine
        }
        
        messageLabel.rac_text <~ viewModel.backgroundText
        
        title = viewModel.title
    }
    
    func addBackgroundLabel() {
        messageLabel = UILabel()
        
        messageLabel.textColor = UIColor.blackColor()
        messageLabel.numberOfLines = 0;
        messageLabel.textAlignment = .Center;
        messageLabel.sizeToFit()
        
        self.tableView.backgroundView = messageLabel;
    }

    // MARK: - Table view data source

    override func numberOfSectionsInTableView(tableView: UITableView) -> Int {
        return 1
    }

    override func tableView(tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return viewModel.mangas.value.count
    }

    
    override func tableView(tableView: UITableView, cellForRowAtIndexPath indexPath: NSIndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCellWithIdentifier("MangaCell", forIndexPath: indexPath) as! MangaTableViewCell

        cell.configure(viewModel.mangas.value[indexPath.row])

        return cell
    }
    
    override func tableView(tableView: UITableView, didSelectRowAtIndexPath indexPath: NSIndexPath) {
        tableView.deselectRowAtIndexPath(indexPath, animated: true)
        performSegueWithIdentifier("MangaDetail", sender: indexPath)
    }
    
    override func tableView(tableView: UITableView, willSelectRowAtIndexPath indexPath: NSIndexPath) -> NSIndexPath? {
        lastSelectedIndexPath = indexPath
        return indexPath
    }
    
    override func tableView(tableView: UITableView, heightForRowAtIndexPath indexPath: NSIndexPath) -> CGFloat {
        return 82
    }
    
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepareForSegue(segue: UIStoryboardSegue, sender: AnyObject?) {
        let controller = segue.destinationViewController as! MangaDetailViewController
        controller.viewModel = self.viewModel.mangaDetailViewModel(sender as! NSIndexPath)
    }

}


extension MangaListViewController: UINavigationControllerDelegate {
    
    func selectedCellSnapshot() -> UIView {
        let cell = tableView.cellForRowAtIndexPath(lastSelectedIndexPath!)!
        let result = cell.snapshotViewAfterScreenUpdates(false)
        result.frame = cell.convertRect(cell.bounds, toView: self.view.superview)
        return result
    }
    
    func navigationController(navigationController: UINavigationController, animationControllerForOperation operation: UINavigationControllerOperation, fromViewController fromVC: UIViewController, toViewController toVC: UIViewController) -> UIViewControllerAnimatedTransitioning? {
        if operation == .Pop {
            return nil
        }
        return transition
    }
}