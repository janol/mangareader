//
//  MangaTableViewCell.swift
//  Mangareader
//
//  Created by Jan Olbrich on 09.01.16.
//  Copyright © 2016 Jan Olbrich. All rights reserved.
//

import UIKit
import AlamofireImage

class MangaTableViewCell: UITableViewCell {

    @IBOutlet weak var coverImageView: UIImageView!
    @IBOutlet weak var titleLabel: UILabel!
    
    
    func configure(manga: Manga) {
        titleLabel.text = manga.name
        guard let coverImage = manga.coverImage else {
            return
        }
        coverImageView.af_setImageWithURL(coverImage)
    }

}
