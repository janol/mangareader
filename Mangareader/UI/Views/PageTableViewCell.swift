//
//  PageTableViewCell.swift
//  Mangareader
//
//  Created by Jan Olbrich on 15.01.16.
//  Copyright © 2016 Jan Olbrich. All rights reserved.
//

import UIKit
import AlamofireImage

class PageTableViewCell: UITableViewCell {

    @IBOutlet weak var pageImageView: UIImageView!
    
    func configure(page: Page) {
        pageImageView.af_setImageWithURL(page.url)
    }


}
