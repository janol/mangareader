//
//  ChapterTableViewCell.swift
//  Mangareader
//
//  Created by Jan Olbrich on 15.01.16.
//  Copyright © 2016 Jan Olbrich. All rights reserved.
//

import UIKit

class ChapterTableViewCell: UITableViewCell {

    @IBOutlet weak var chapterNumberLabel: UILabel!
    @IBOutlet weak var chapterNameLabel: UILabel!
    
    func configure(chapter: Chapter) {
        self.chapterNumberLabel.text = chapter.chapterNumber
        self.chapterNameLabel.text = chapter.name
    }
    
}
