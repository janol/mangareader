//
//  MangaDetailViewModel.swift
//  Mangareader
//
//  Created by Jan Olbrich on 13.01.16.
//  Copyright © 2016 Jan Olbrich. All rights reserved.
//

import Foundation
import ReactiveCocoa

struct MangaDetailViewModel {
    private var manga: MutableProperty<Manga>
    private var model = MangaStorage()
    
    let title = MutableProperty<String>("")
    let author = MutableProperty<String>("")
    let coverURL = MutableProperty<NSURL>(NSURL())
    let mangaDescription = MutableProperty<String>("")
    let chapters = MutableProperty<[Chapter]>([])
    
    init(object: Manga) {
        manga = MutableProperty<Manga>(object)
        setupBindings()
        
        updateData()
    }
    
    func setupBindings() {
        manga.producer.startWithNext { manga in
            self.title.value = manga.name
            
            if let author = manga.author {
                self.author.value = author
            }
            
            if let cover = manga.coverImage {
                self.coverURL.value = cover
            }
            
            if let description = manga.description {
                self.mangaDescription.value = description
            }
            
            if let chapters = manga.chapters {
                self.chapters.value = chapters
            }
        }
    }
    
    mutating func updateData() {
        model.mangaDetailsSignal(manga.value).on(completed: {
            self.model.mangaChaptersSignal(self.manga.value).startWithNext { manga in
                self.manga.value = manga
            }
        }).startWithNext { manga in
            self.manga.value = manga
        }
    }
    
    func pagesReaderViewModel(index: NSIndexPath) -> PagesReaderViewModel {
        return PagesReaderViewModel(object: chapters.value[index.row])
    }
}