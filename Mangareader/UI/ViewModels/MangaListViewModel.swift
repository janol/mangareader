//
//  MangaListViewModel.swift
//  Mangareader
//
//  Created by Jan Olbrich on 07.01.16.
//  Copyright © 2016 Jan Olbrich. All rights reserved.
//

import Foundation
import ReactiveCocoa

struct MangaListViewModel {
    
    let title = "Mangas"
    let updateSignal = MutableProperty<Bool>(false)
    let tableViewSeparatorHidden = MutableProperty<Bool>(false)
    let mangas = MutableProperty<[Manga]>([])
    let backgroundText = MutableProperty<String>("")
    
    var updateAction: Action<String, String, NoError>!
    
    
    private var model = MangaStorage()

    
    init() {
        updateAction = Action<String, String, NoError>() { _ in
            return SignalProducer { sink, _ in
                self.updateSignal.value = true
                self.model.allMangaSignal().startWithNext { mangaList in
                    self.mangas.value = mangaList
                    self.updateSignal.value = false
                    sink.sendCompleted()
                }
            }
        }
        
        backgroundText <~ updateSignal.producer.map { updating in
            return updating ? "Mangas are loading. Please wait." : "No Mangas are currently available. Please pull down to refresh."
        }
        
        tableViewSeparatorHidden <~ mangas.producer.map { mangaList in
            return mangaList.count <= 0
        }
    }
    
    func mangaDetailViewModel(index: NSIndexPath) -> MangaDetailViewModel {
        return MangaDetailViewModel(object: mangas.value[index.row])
    }
    
}