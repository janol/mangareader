//
//  PagesReaderViewModel.swift
//  Mangareader
//
//  Created by Jan Olbrich on 15.01.16.
//  Copyright © 2016 Jan Olbrich. All rights reserved.
//

import Foundation
import ReactiveCocoa

struct PagesReaderViewModel {
    private let chapter: MutableProperty<Chapter>!
    private var model = MangaStorage()
    
    let pages = MutableProperty<[Page]>([])
    
    init(object: Chapter) {
        chapter = MutableProperty<Chapter>(object)
        
        setupBindings()
        loadPages()
    }
    
    func setupBindings() {
        chapter.producer.startWithNext { chapter in
            if let pages = chapter.pages {
                self.pages.value = pages
            }
        }
    }
    
    mutating func loadPages() {
        model.chapterPagesSignal(chapter.value).startWithNext { chapter in
            self.chapter.value = chapter
        }
    }
}