//
//  MangaSiteAPI.swift
//  Mangareader
//
//  Created by Jan Olbrich on 04.01.16.
//  Copyright © 2016 Jan Olbrich. All rights reserved.
//

import Foundation

struct MangaSiteAPI {
    
    //MARK: URLs
    
    static let BaseURL = ""
    
    static let PagesBaseURL = MangaSiteAPI.BaseURL + ""
    
    static let CoverBaseURL = MangaSiteAPI.PagesBaseURL + ""
    
    static let APIBaseURL = MangaSiteAPI.BaseURL + ""
    
    static let MangaListURL = MangaSiteAPI.APIBaseURL + ""
    
    static let MangaDetailsURL = MangaSiteAPI.APIBaseURL + ""
    
    static let MangaChapterListURL = MangaSiteAPI.APIBaseURL + ""
    
    static let ChapterPagesListURL = MangaSiteAPI.APIBaseURL + ""
    
    static let UserFavoritesURL = MangaSiteAPI.APIBaseURL + ""

    
    //MARK: Parameters
    
    static let MangaID = ""
    
    static let ClientID = ""
    
    static let ChapterID = ""
    
    
    //MARK: JSON IDs
    
    static let JSONMangaRoot = ""
    
    static let JSONMangaID = ""
    
    static let JSONMangaName = ""
    
    static let JSONMangaImage = ""
    
    static let JSONMangaAuthor = ""
    
    static let JSONMangaDescription = ""
    
    static let JSONMangaLike = ""
    
    static let JSONMangaFavorite = ""
    
    static let JSONMangaStatus = ""
    
    static let JSONChapterRoot = ""
    
    static let JSONChapterID = ""
    
    static let JSONChapterName = ""
    
    static let JSONChapterNumber = ""
    
    static let JSONPageRoot = ""
    
    static let JSONPageURL = ""
}