//
//  String+URLEncoding.swift
//  Mangareader
//
//  Created by Jan Olbrich on 05.01.16.
//  Copyright © 2016 Jan Olbrich. All rights reserved.
//

import Foundation

extension String {
    var URLEncoding: String {
        get {
            return self.stringByAddingPercentEncodingWithAllowedCharacters(.URLQueryAllowedCharacterSet())!
        }
    }
}