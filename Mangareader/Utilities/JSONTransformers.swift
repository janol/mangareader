//
//  JSONTransformer.swift
//  Mangareader
//
//  Created by Jan Olbrich on 04.01.16.
//  Copyright © 2016 Jan Olbrich. All rights reserved.
//

import Foundation
import JSONCodable

struct JSONTransformers {
    
    static let CoverImageStringToNSURL = JSONTransformer<String, NSURL>(
        decoding: {return NSURL(string: MangaSiteAPI.CoverBaseURL + $0.URLEncoding)},
        encoding: {$0.absoluteString})
    
    static let PagesImageStringToNSURL = JSONTransformer<String, NSURL>(decoding: {
        return NSURL(string: MangaSiteAPI.PagesBaseURL + $0.URLEncoding)
        }, encoding: {$0.absoluteString})
    
    static let StringToBool = JSONTransformer<NSString, Bool>(decoding: {NSString(string: $0).boolValue}, encoding:{ $0 ? "true" : "false"})
    
}