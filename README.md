# Mangareader

This iOS project creates the basic environment to read Mangas. The API and any reference to the used Manga - Webpage has been removed.

## How do I get set up?
To compile this code you need to update the Carthage produced libraries. This will currently create only a build for the simulator, as the x86 libs are not removed. Add the corresponding Carthage script to the build process.

## Licenses
`Mangareader` is available under the MIT license. See the LICENSE file for more info.

Thanks to:

* [Quick](https://github.com/Quick/Quick), [License](https://github.com/Quick/Quick/blob/master/LICENSE)
* [Nimble](https://github.com/Quick/Nimble), [License](https://github.com/Quick/Nimble/blob/master/LICENSE.md)
* [OHHTTPStubs](https://github.com/AliSoftware/OHHTTPStubs), [License](https://github.com/AliSoftware/OHHTTPStubs/blob/master/LICENSE)
* [JSONCodable](https://github.com/matthewcheok/JSONCodable), [License](https://github.com/matthewcheok/JSONCodable/blob/master/LICENSE)
* [Alamofire](https://github.com/Alamofire/Alamofire), [License](https://github.com/Alamofire/Alamofire/blob/master/LICENSE)
* [AlamofireImage](https://github.com/Alamofire/AlamofireImage), [License](https://github.com/Alamofire/AlamofireImage/blob/master/LICENSE)
* [ReactiveCocoa](https://github.com/ReactiveCocoa/ReactiveCocoa), [License](https://github.com/ReactiveCocoa/ReactiveCocoa/blob/master/LICENSE.md)
* [EasyAnimation](https://github.com/icanzilb/EasyAnimation), [License](https://github.com/icanzilb/EasyAnimation/blob/master/LICENSE)
