//
//  PageReaderTableViewControllerTests.swift
//  Mangareader
//
//  Created by Jan Olbrich on 15.01.16.
//  Copyright © 2016 Jan Olbrich. All rights reserved.
//

import Quick
import Nimble
import OHHTTPStubs

@testable import Mangareader

class PageReaderTableViewControllerTests: QuickSpec {
    
    override func spec() {
        var sut: PageReaderViewController!
        var viewModel: PagesReaderViewModel!
        let chapter = try! Chapter(object: MangaSiteDemoData.singleChapter)
        
        beforeEach {
            OHHTTPStubs.stubMangaPages(MangaSiteDemoData.mangaPages)
            OHHTTPStubs.stubMangaCover(UIImage(named: "cover1", inBundle: NSBundle(forClass: self.dynamicType), compatibleWithTraitCollection: nil)!)
            
            let storyboard = UIStoryboard(name: "Main", bundle: nil)
            sut = storyboard.instantiateViewControllerWithIdentifier("PageReaderViewController") as! PageReaderViewController
            sut.loadView()
            
            viewModel = PagesReaderViewModel(object: chapter)
            sut.viewModel = viewModel
            sut.viewDidLoad()
            sut.viewWillAppear(true)
        }
        
        afterEach {
            OHHTTPStubs.removeAllStubs()
        }
        
        context("after ViewWillAppear") {
            it("should provide 1 section") {
                expect(sut.numberOfSectionsInTableView(UITableView())) == 1
            }
            
            it("should provide two entries in the tableview") {
                expect(sut.tableView(UITableView(), numberOfRowsInSection: 0)).toEventually(equal(2))
            }
            
            it("should return MangaTableViewCells") {
                //not the best way, but due to concurrency on threat it's a solution to just set the viewModel data
                let pages = try! [Page](JSONArray: MangaSiteDemoData.mangaPages["pages"]!)
                sut.viewModel.pages.value = pages
                
                expect(sut.tableView(sut.tableView, cellForRowAtIndexPath: NSIndexPath(forRow: 0, inSection: 0))).to(beAnInstanceOf(PageTableViewCell))
            }
            
            it("should call reloadData on tableView when the data is present") {
                let tableMock = MockUITableView()
                sut.tableView = tableMock
                let mock = sut.tableView as! MockUITableView
                expect(mock.didCallReloadData).toEventually(equal(true))
            }
            
            it("should have configured the PageTableViewCell") {
                let chapters = try! [Page](JSONArray: MangaSiteDemoData.mangaPages["pages"]!)
                sut.viewModel.pages.value = chapters
                let cell = sut.tableView(sut.tableView, cellForRowAtIndexPath: NSIndexPath(forRow: 0, inSection: 0)) as! PageTableViewCell
                expect(cell.pageImageView.image).toNotEventually(equal(nil))
            }
            
            it("should return the correct cell height") {
                expect(sut.tableView(UITableView(), heightForRowAtIndexPath: NSIndexPath(forRow: 0, inSection: 0))).to(equal(sut.view.bounds.height))
            }
        }
    }
}