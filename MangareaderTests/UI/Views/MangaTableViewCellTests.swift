//
//  MangaTableViewCellTests.swift
//  Mangareader
//
//  Created by Jan Olbrich on 12.01.16.
//  Copyright © 2016 Jan Olbrich. All rights reserved.
//

import Quick
import Nimble
import OHHTTPStubs

@testable import Mangareader

class MangaTableViewCellTests: QuickSpec {
    class MockController: UITableViewController {
        let data = [""]
        
        override func numberOfSectionsInTableView(tableView: UITableView) -> Int {
            return 1
        }
        
        override func tableView(tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
            return data.count
        }
    }
    
    override func spec() {
        var sut: MangaTableViewCell!
        var mockController: MockUITableViewController!
        let manga = try! Manga(object: MangaSiteDemoData.singleManga)
        
        
        beforeEach() {
            OHHTTPStubs.stubMangaCover(UIImage(named: "cover1", inBundle: NSBundle(forClass: self.dynamicType), compatibleWithTraitCollection: nil)!)
            
            mockController = MockUITableViewController()
            mockController.registerCell("MangaTableViewCell", reuseIdentifier: "MangaCell")
            
            sut = mockController.tableView.dequeueReusableCellWithIdentifier("MangaCell", forIndexPath: NSIndexPath(forRow: 0, inSection: 0)) as! MangaTableViewCell
            sut.configure(manga)
        }
        
        afterEach() {
            OHHTTPStubs.removeAllStubs()
        }
        
        context("after initialization") {
            it("should have the correct title") {
                expect(sut.titleLabel.text) == manga.name
            }
            
            it("should download and set the image") {
                expect(sut.coverImageView.image).toNotEventually(equal(nil))
            }
        }
    }
}