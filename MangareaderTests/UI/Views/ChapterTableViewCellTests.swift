//
//  ChapterTableViewCellTests.swift
//  Mangareader
//
//  Created by Jan Olbrich on 15.01.16.
//  Copyright © 2016 Jan Olbrich. All rights reserved.
//

import Quick
import Nimble

@testable import Mangareader

class ChapterTableViewTests: QuickSpec {
    override func spec() {
        var sut: ChapterTableViewCell!
        var mockController: MockUITableViewController!
        let chapter = try! Chapter(object: MangaSiteDemoData.singleChapter)
        
        
        beforeEach() {
            mockController = MockUITableViewController()
            mockController.registerCell("ChapterTableViewCell", reuseIdentifier: "ChapterCell")
            
            sut = mockController.tableView.dequeueReusableCellWithIdentifier("ChapterCell", forIndexPath: NSIndexPath(forRow: 0, inSection: 0)) as! ChapterTableViewCell
            sut.configure(chapter)
        }
        
        context("after initialization") {
            it("should have the correct name") {
                expect(sut.chapterNameLabel.text) == chapter.name
            }
            
            it("should have the correct chapter number") {
                expect(sut.chapterNumberLabel.text) == chapter.chapterNumber
            }
        }
    }
}