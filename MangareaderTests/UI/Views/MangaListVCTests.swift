//
//  MangaListVCTests.swift
//  Mangareader
//
//  Created by Jan Olbrich on 09.01.16.
//  Copyright © 2016 Jan Olbrich. All rights reserved.
//

import Quick
import Nimble
import OHHTTPStubs
import JSONCodable
import ReactiveCocoa

@testable import Mangareader

class MangaListVCTests: QuickSpec {
    class MockController: MangaListViewController {
        var data:[Manga] = []
        
        var didCallPerformeSegueWithIdentifier = false
        var didCallSelectedCellSnapshot = false
        
        override func numberOfSectionsInTableView(tableView: UITableView) -> Int {
            return 1
        }
        
        override func tableView(tableView: UITableView, cellForRowAtIndexPath indexPath: NSIndexPath) -> UITableViewCell {
            let cell = tableView.dequeueReusableCellWithIdentifier("MangaCell", forIndexPath: indexPath) as! MangaTableViewCell
            cell.configure(data[indexPath.row])
            return cell
        }
        
        override func tableView(tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
            return data.count
        }
        
        override func performSegueWithIdentifier(identifier: String, sender: AnyObject?) {
            didCallPerformeSegueWithIdentifier = true
        }
        
        override func selectedCellSnapshot() -> UIView {
            didCallSelectedCellSnapshot = true
            return super.selectedCellSnapshot()
        }
    }

    
    override func spec() {
        var sut: MangaListViewController!
        
        beforeEach() {
            OHHTTPStubs.stubMangaList(MangaSiteDemoData.mangaList)
            
            sut = MangaListViewController()
            sut.viewDidLoad()
        }
        
        afterEach() {
            OHHTTPStubs.removeAllStubs()
        }
        
        context("after viewDidLoad") {
            it("should provide 1 section") {
                expect(sut.numberOfSectionsInTableView(UITableView())) == 1
            }
            
            it("should return MangaTableViewCells") {
                //not the best way, but due to concurrency on threat it's a solution to just set the viewModel data
                let mangas = try! [Manga](JSONArray: MangaSiteDemoData.mangaList["manga"]!)
                sut.viewModel.mangas.value = mangas
                
                expect(sut.tableView(sut.tableView, cellForRowAtIndexPath: NSIndexPath(forRow: 0, inSection: 0))).to(beAnInstanceOf(MangaTableViewCell))
            }
            
            it("should return cell height 82") {
                expect(sut.tableView(UITableView(), heightForRowAtIndexPath: NSIndexPath(forRow: 0, inSection: 0))) == 82
            }
            
            
            it("should have the title Mangas") {
                expect(sut.title) == "Mangas"
            }
            
            it("should have configured the MangaTableViewCell") {
                let mangas = try! [Manga](JSONArray: MangaSiteDemoData.mangaList["manga"]!)
                sut.viewModel.mangas.value = mangas
                let cell = sut.tableView(sut.tableView, cellForRowAtIndexPath: NSIndexPath(forRow: 0, inSection: 0)) as! MangaTableViewCell
                expect(cell.titleLabel.text) != ""
            }
            
            it("should have the navigation controller delegate set to self") {
                let navController = UINavigationController(rootViewController: sut)
                sut.viewWillAppear(true)
                expect(navController.delegate).notTo(beNil())
            }
            
            it("should have no separator displayed") {
                expect(sut.tableView.separatorStyle) == UITableViewCellSeparatorStyle.None
            }
            
            it("should display the no data background text") {
                expect(sut.messageLabel.text) == "No Mangas are currently available. Please pull down to refresh."
            }
        }
        
        context("when refreshing the tableView") {
            beforeEach {
                let action = CocoaAction(sut.viewModel.updateAction, input:"")
                action.execute("")
            }
            
            it("should provide two entries in the tableview") {
                expect(sut.tableView(UITableView(), numberOfRowsInSection: 0)).toEventually(equal(2))
            }
            
            it("should call reloadData on tableView when the data is present") {
                let tableMock = MockUITableView()
                sut.tableView = tableMock
                let mock = sut.tableView as! MockUITableView
                expect(mock.didCallReloadData).toEventually(equal(true))
            }
            
            it("should display the refreshControl") {
                expect(sut.refreshControl).notTo(beNil())
            }
            
            it("should display the refreshing background text") {
                expect(sut.messageLabel.text) == "Mangas are loading. Please wait."
            }
        }
        
        context("after refreshing the tableView") {
            it("should have a SeparatorStyle set to single") {
                let action = CocoaAction(sut.viewModel.updateAction, input:"")
                action.execute("")
                expect(sut.tableView.separatorStyle).toEventually(equal(UITableViewCellSeparatorStyle.SingleLine))
            }
        }
        
        context("after selecting a cell") {
            it("should call performSegueWithIdentifier") {
                let mockController = MockController()
                mockController.tableView(mockController.tableView, didSelectRowAtIndexPath: NSIndexPath(forRow: 0, inSection: 0))
                expect(mockController.didCallPerformeSegueWithIdentifier).toEventually(beTrue())
            }
            
            it("should save the last selected indexPath") {
                let indexPath = NSIndexPath(forRow: 0, inSection: 0)
                sut.tableView(UITableView(), willSelectRowAtIndexPath: indexPath)
                expect(sut.lastSelectedIndexPath).to(equal(indexPath))
            }
            
            it("should provide an image screenshot") {
                let mockController = MockController()
                mockController.data = try! [Manga](jsonMangaDict: MangaSiteDemoData.mangaList)
                mockController.lastSelectedIndexPath = NSIndexPath(forRow: 0, inSection: 0)
                mockController.selectedCellSnapshot()
                expect(mockController.didCallSelectedCellSnapshot) == true
            }
        }
    }
}