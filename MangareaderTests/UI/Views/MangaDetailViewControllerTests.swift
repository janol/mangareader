//
//  MangaDetailViewControllerTests.swift
//  Mangareader
//
//  Created by Jan Olbrich on 13.01.16.
//  Copyright © 2016 Jan Olbrich. All rights reserved.
//

import Quick
import Nimble
import OHHTTPStubs

@testable import Mangareader

class MangaDetailViewControllerTests: QuickSpec {
    class MangaDetailMockController: MangaDetailViewController {
        let data = [""]
        
        
        var didCallPerformeSegueWithIdentifier = false
        
        override func numberOfSectionsInTableView(tableView: UITableView) -> Int {
            return 1
        }
        
        override func tableView(tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
            return data.count
        }
        
        override func performSegueWithIdentifier(identifier: String, sender: AnyObject?) {
            didCallPerformeSegueWithIdentifier = true
        }
    }
    
    override func spec() {
        var sut: MangaDetailViewController!
        var viewModel: MangaDetailViewModel!
        let manga = try! Manga(object: MangaSiteDemoData.singleManga)
        
        beforeEach() {
            OHHTTPStubs.stubMangaChapters(MangaSiteDemoData.mangaChapters)
            
            let storyboard = UIStoryboard(name: "Main", bundle: nil)
            sut = storyboard.instantiateViewControllerWithIdentifier("MangaDetailViewController") as! MangaDetailViewController
            sut.loadView()
            viewModel = MangaDetailViewModel(object: manga)
            sut.viewModel = viewModel
            sut.viewWillAppear(true)
        }
        
        afterEach() {
            OHHTTPStubs.removeAllStubs()
        }
        
        context("after viewDidLoad") {
            it("should have the correct title") {
                expect(sut.titleLabel.text) == viewModel.title.value
            }
            
            it("should display the correct author") {
                expect(sut.authorLabel.text) == viewModel.author.value
            }
            
            it("should display the correct description") {
                expect(sut.descriptionLabel.text) == viewModel.mangaDescription.value
            }
            
            it("should provide 1 section") {
                expect(sut.numberOfSectionsInTableView(UITableView())) == 1
            }
            
            it("should provide two entries in the tableview") {
                expect(sut.tableView(UITableView(), numberOfRowsInSection: 0)).toEventually(equal(2))
            }
            
            it("should return MangaTableViewCells") {
                //not the best way, but due to concurrency on threat it's a solution to just set the viewModel data
                let chapters = try! [Chapter](JSONArray: MangaSiteDemoData.mangaChapters["chapters"]!)
                sut.viewModel.chapters.value = chapters
                
                expect(sut.tableView(sut.chaptersTableView, cellForRowAtIndexPath: NSIndexPath(forRow: 0, inSection: 0))).to(beAnInstanceOf(ChapterTableViewCell))
            }
            
            it("should call reloadData on tableView when the data is present") {
                let tableMock = MockUITableView()
                sut.chaptersTableView = tableMock
                let mock = sut.chaptersTableView as! MockUITableView
                expect(mock.didCallReloadData).toEventually(equal(true))
            }
            
            it("should have configured the ChapterTableViewCell") {
                let chapters = try! [Chapter](JSONArray: MangaSiteDemoData.mangaChapters["chapters"]!)
                sut.viewModel.chapters.value = chapters
                let cell = sut.tableView(sut.chaptersTableView, cellForRowAtIndexPath: NSIndexPath(forRow: 0, inSection: 0)) as! ChapterTableViewCell
                expect(cell.chapterNameLabel.text) != ""
            }
            
            it("should have the navigation controller delegate set to nil") {
                let navController = UINavigationController(rootViewController: sut)
                sut.viewWillAppear(true)
                expect(navController.delegate).to(beNil())
            }
        }
        
        context("clicking a cell") {
            it("should call performSegueWithIdentifier") {
                let mockController = MangaDetailMockController()
                mockController.tableView(UITableView(), didSelectRowAtIndexPath: NSIndexPath(forRow: 0, inSection: 0))
                expect(mockController.didCallPerformeSegueWithIdentifier).toEventually(beTrue())
            }
        }
    }
}