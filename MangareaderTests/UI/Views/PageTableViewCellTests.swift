//
//  PageTableViewCellTests.swift
//  Mangareader
//
//  Created by Jan Olbrich on 15.01.16.
//  Copyright © 2016 Jan Olbrich. All rights reserved.
//

import Quick
import Nimble
import OHHTTPStubs

@testable import Mangareader

class PageTableViewCellTests: QuickSpec {
    override func spec() {
        var sut: PageTableViewCell!
        var mockController: MockUITableViewController!
        let page = try! Page(object: MangaSiteDemoData.singlePage)
        
        beforeEach {
            OHHTTPStubs.stubMangaCover(UIImage(named: "cover1", inBundle: NSBundle(forClass: self.dynamicType), compatibleWithTraitCollection: nil)!)
            mockController = MockUITableViewController()
            mockController.registerCell("PageTableViewCell", reuseIdentifier: "PageCell")
            
            sut = mockController.tableView.dequeueReusableCellWithIdentifier("PageCell", forIndexPath: NSIndexPath(forRow: 0, inSection: 0)) as! PageTableViewCell
            sut.configure(page)
        }
        
        afterEach {
            OHHTTPStubs.removeAllStubs()
        }
        
        context("after configuration") {
            it("should eventually show an image") {
                expect(sut.pageImageView.image).toNotEventually(equal(nil))
            }
        }
    }
}