//
//  MangaListViewModelTests.swift
//  Mangareader
//
//  Created by Jan Olbrich on 07.01.16.
//  Copyright © 2016 Jan Olbrich. All rights reserved.
//

import Quick
import Nimble
import OHHTTPStubs
import ReactiveCocoa

@testable import Mangareader

class MangaListViewModelTest: QuickSpec {
    override func spec() {
        var sut: MangaListViewModel!
        
        beforeEach() {
            OHHTTPStubs.stubMangaList(MangaSiteDemoData.mangaList)
            
            sut = MangaListViewModel()
        }
        
        afterEach() {
            OHHTTPStubs.removeAllStubs()
        }
        
        context("after initialization") {
            it("should have a title named Mangas") {
                expect(sut.title).to(equal("Mangas"))
            }
            
            it("should have the tableViewSeparatorHidden set to false") {
                expect(sut.tableViewSeparatorHidden.value) == true
            }
            
            it("should provide the correct backgroundtext") {
                expect(sut.backgroundText.value) == "No Mangas are currently available. Please pull down to refresh."
            }
            
            it("should have the updatingSignale set to false") {
                expect(sut.updateSignal.value) == false
            }
        }
        
        context("while updating") {
            beforeEach {
                let action = CocoaAction(sut.updateAction, input: "")
                action.execute("")
            }
            
            it("should provide eventually a list of mangas") {
                expect(sut.mangas.value.count).toEventually(beGreaterThan(0))
            }
            
            it("should set updateSignal to true") {
                expect(sut.updateSignal.value) == true
            }
            
            it("should provide the correct backgroundtext") {
                expect(sut.backgroundText.value) == "Mangas are loading. Please wait."
            }
        }
        
        context("after updating") {
            it("should set updateSignal to false") {
                expect(sut.updateSignal.value).toEventually(equal(false))
            }
            
            it("should set tableViewSeparatorHidden to true") {
                expect(sut.tableViewSeparatorHidden.value) == true
            }
        }
        
        context("when performing a segue") {
            it("should create a MangaDetailViewModel") {
                sut.mangas.value = [try! Manga(object: MangaSiteDemoData.singleManga)]
                expect(sut.mangaDetailViewModel(NSIndexPath(forRow: 0, inSection: 0)).title.value) == "\"Aoi\" - Hikaru ga Chikyuu ni Itakoro......"
            }
        }
    }
}