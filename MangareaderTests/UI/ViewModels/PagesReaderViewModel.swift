//
//  PagesReaderViewModel.swift
//  Mangareader
//
//  Created by Jan Olbrich on 15.01.16.
//  Copyright © 2016 Jan Olbrich. All rights reserved.
//

import Quick
import Nimble
import OHHTTPStubs

@testable import Mangareader

class PagesReaderViewModelTests: QuickSpec {
    override func spec() {
        var chapter:Chapter!
        var sut: PagesReaderViewModel!
        
        beforeEach {
            OHHTTPStubs.stubMangaPages(MangaSiteDemoData.mangaPages)
            chapter = try! Chapter(object: MangaSiteDemoData.singleChapter)
            sut = PagesReaderViewModel(object: chapter)
        }
        
        afterEach {
            OHHTTPStubs.removeAllStubs()
        }
        
        context("after initialization") {
            it("should provide an empty list in the beginning") {
                expect(sut.pages.value.count) == 0
            }
            
            it("should provide eventually the whole pageslist") {
                expect(sut.pages.value.count).toEventually(equal(2), timeout:1)
            }
        }
    }
}