//
//  MangaDetailViewModelTests.swift
//  Mangareader
//
//  Created by Jan Olbrich on 13.01.16.
//  Copyright © 2016 Jan Olbrich. All rights reserved.
//

import Quick
import Nimble
import OHHTTPStubs

@testable import Mangareader

class MangaDetailViewModelTests: QuickSpec {
    override func spec() {
        var sut: MangaDetailViewModel!
        let manga = try! Manga(object: MangaSiteDemoData.minimalManga)
        let resultManga = try! Manga(object: MangaSiteDemoData.singleManga)
        
        beforeEach() {
            OHHTTPStubs.stubMangaChapters(MangaSiteDemoData.mangaChapters)
            OHHTTPStubs.stubMangaDetails(MangaSiteDemoData.mangaDetail)
            OHHTTPStubs.stubMangaPages(MangaSiteDemoData.mangaPages)
            sut = MangaDetailViewModel(object: manga)
        }
        
        afterEach() {
            OHHTTPStubs.removeAllStubs()
        }
        
        context("after initialization") {
            it("should provide a property with the correct title") {
                expect(sut.title.value) == manga.name
            }
            
            it("should povide a property with the correct author") {
                expect(sut.author.value).toEventually(equal(resultManga.author!))
            }
            
            it("should provide a property with the correct coverURL") {
                expect(sut.coverURL.value).toEventually(equal(resultManga.coverImage!))
            }
            
            it("should provide a description") {
                expect(sut.mangaDescription.value).toEventually(equal(resultManga.description!))
            }
            
            it("should list the mangas chapters") {
                expect(sut.chapters.value.count).toEventually(equal(2))
            }
        }
        
        context("when performing a segue") {
            it("should create a PagesReaderViewModel") {
                sut.chapters.value = [try! Chapter(object: MangaSiteDemoData.singleChapter)]
                let vm = sut.pagesReaderViewModel(NSIndexPath(forRow: 0, inSection: 0))
                expect(vm.pages.value.count).toEventually(beGreaterThan(0))
            }
        }
    }
}