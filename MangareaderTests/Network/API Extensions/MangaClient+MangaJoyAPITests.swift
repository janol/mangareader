//
//  MangaClient+MangaSiteAPITests.swift
//  Mangareader
//
//  Created by Jan Olbrich on 03.01.16.
//  Copyright © 2016 Jan Olbrich. All rights reserved.
//

import Quick
import Nimble
import OHHTTPStubs

@testable import Mangareader

class MangaClient_MangaSiteAPITests: QuickSpec {
    override func spec() {
        
        var client: MangaClient!
        var value: Dictionary<String, NSObject>!
        
        beforeEach { () -> () in
            OHHTTPStubs.stubMangaList(MangaSiteDemoData.mangaList)
            
            OHHTTPStubs.stubMangaDetails(MangaSiteDemoData.mangaDetail)
            
            OHHTTPStubs.stubMangaChapters(MangaSiteDemoData.mangaChapters)
            
            OHHTTPStubs.stubUserFavorites(MangaSiteDemoData.clientFavorites)
            
            OHHTTPStubs.stubMangaPages(MangaSiteDemoData.mangaPages)
        }
        
        beforeEach { () -> () in
            client = MangaClient()
        }
        
        afterEach { () -> () in
            OHHTTPStubs.removeAllStubs()
        }
        
        it("should fetch the JSON MangaList") {
            client.mangaList().startWithNext({ jsonResult in
                value = jsonResult as! Dictionary<String, NSObject>
            })
            expect(value).toEventually(equal(MangaSiteDemoData.mangaList))
        }
        
        it("should fetch the details for Mangas") {
            client.detailsForMangaWithID("3", clientID: "abc").startWithNext({ jsonResult in
                value = jsonResult as! Dictionary<String, NSObject>
            })
            expect(value).toEventually(equal(MangaSiteDemoData.mangaDetail))
        }
        
        it("should fetch the chapters for a Manga") {
            client.chaptersForMangaWithID("13889").startWithNext({ jsonResult in
                value = jsonResult as! Dictionary<String, NSObject>
            })
            expect(value).toEventually(equal(MangaSiteDemoData.mangaChapters))
        }
        
        it("should fetch the favorites for a User") {
            client.favoritesForClientID("abc").startWithNext({ jsonResult in
                value = jsonResult as! Dictionary<String, NSObject>
            })
            expect(value).toEventually(equal(MangaSiteDemoData.clientFavorites))
        }
        
        it("should fetch the pages for a Chapter") {
            client.pagesForChapterWithID("198963").startWithNext({ jsonResult in
                value = jsonResult as! Dictionary<String, NSObject>
            })
            expect(value).toEventually(equal(MangaSiteDemoData.mangaPages))
        }
    }
}