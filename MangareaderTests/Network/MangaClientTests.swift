//
//  MangaClientTests.swift
//  Mangareader
//
//  Created by Jan Olbrich on 02.01.16.
//  Copyright © 2016 Jan Olbrich. All rights reserved.
//

import Quick
import Nimble
import Alamofire
import OHHTTPStubs
import ReactiveCocoa

@testable import Mangareader

class MangaClientTests: QuickSpec {
    override func spec() {
        var client: MangaClient!
        beforeEach { () -> () in
            client = MangaClient()
            
            stub(isHost("qrng.anu.edu.au") && isPath("/API/jsonI.php")) {_ in
                return OHHTTPStubsResponse( JSONObject: ["type": "uint8", "length": 1, "data": [92], "success": true], statusCode: 200, headers: ["ContentType": "application/json"])
            }
            
            stub(isHost("qrng.anu.edu.au") && isPath("/API/error.php")) {_ in
                let notConnectedError = NSError(domain:NSURLErrorDomain, code:Int(CFNetworkErrors.CFURLErrorNotConnectedToInternet.rawValue), userInfo:nil)
                return OHHTTPStubsResponse(error:notConnectedError)
            }
        }
        
        context("given an address") {
            it("should return the correct value") {
                var value: Dictionary<String, NSObject>!
                client.requestWithMethod(.GET, address: "https://qrng.anu.edu.au/API/jsonI.php" , parameters: ["length": "1", "type": "uint8"]).startWithNext({ result in
                    value = result as! Dictionary<String, NSObject>
                })
                
                
                expect(value).toEventually(equal(["type": "uint8", "length": 1, "data": [92], "success": true]))
            }
            
            it("should return an error, if not connected to the internet") {
                var value: NSError!
                client.requestWithMethod(.GET, address: "https://qrng.anu.edu.au/API/error.php" , parameters: ["":""]).on(failed: { (error) -> () in
                    value = error
                })
                    .startWithNext({ result in
                    expect(false)
                })
                
                expect(value).toEventually(equal(NSError(domain:NSURLErrorDomain, code:Int(CFNetworkErrors.CFURLErrorNotConnectedToInternet.rawValue), userInfo:nil)))
            }
        }
    }
}
