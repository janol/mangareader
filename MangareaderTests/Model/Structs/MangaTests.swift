//
//  MangaTests.swift
//  Mangareader
//
//  Created by Jan Olbrich on 01.01.16.
//  Copyright © 2016 Jan Olbrich. All rights reserved.
//

import Quick
import Nimble

@testable import Mangareader

class MangaTests: QuickSpec {
    override func spec() {
        context("provided a valid Manga JSON string") {
            
            it("should create a valid Object") {
                let obj = [MangaSiteAPI.JSONMangaID: "198963", MangaSiteAPI.JSONMangaName: "Test12"]
                expect{try Manga(object: obj)}.notTo(throwError())
            }
            
            it("should contain the correct values") {
                let manga = try! Manga(object: MangaSiteDemoData.singleManga)
                expect(manga.id).to(equal("3"))
                expect(manga.name).to(equal("\"Aoi\" - Hikaru ga Chikyuu ni Itakoro......"))
                expect(manga.coverImage).to(equal(NSURL(string: MangaSiteAPI.CoverBaseURL + "cover/Aoi-Hikaru-ga-Chikyuu-ni-Itakoro.jpg")))
                expect(manga.author).to(equal("NOMURA Mizuki, YAMASAKI Fuua, CHUN"))
                expect(manga.description).to(equal("The story starts with the death of Hikaru Mikado. That guy was a natural charmer,[...]"))
                expect(manga.like).to(equal(false))
                expect(manga.favorite) == true
                expect(manga.status) == "1"
            }
            
            it("should be able to convert unicode symbols into urls") {
                let problemManga = [MangaSiteAPI.JSONMangaID:"63", MangaSiteAPI.JSONMangaName:"13nichi wa Kin’youbi?", MangaSiteAPI.JSONMangaImage:"cover/13nichi-wa-Kin’youbi.jpg", MangaSiteAPI.JSONMangaAuthor:"Enomoto Chizuru", MangaSiteAPI.JSONMangaStatus:"1"]

                let manga = try! Manga(object: problemManga)
                expect(manga.coverImage?.host) != nil
            }
        }
        
        context("provided a broken Manga JSON string") {
            it("should throw an exception if a mandatory type is missing") {
                let obj = [MangaSiteAPI.JSONMangaID: "198963", "chapter_number": "12"]
                expect{try Manga(object: obj)}.to(throwError())
            }
        }
        
        context("passing a chapterList") {
            it("should add the chapter objects") {
                let obj = [MangaSiteAPI.JSONMangaID: "198963", MangaSiteAPI.JSONMangaName: "Test12"]

                var manga = try! Manga(object: obj)
                try! manga.addChapters(MangaSiteDemoData.mangaChapters)
                expect(manga.chapters?.count) == 2
            }
        }
        
        context("passing an invalid JSON chapterList") {
            it("should throw an exception") {
                let obj = [MangaSiteAPI.JSONMangaID: "198963", MangaSiteAPI.JSONMangaName: "Test12"]
                let chapters = [MangaSiteAPI.JSONChapterRoot: [[MangaSiteAPI.JSONChapterID: "212613", MangaSiteAPI.JSONChapterNumber: "12", MangaSiteAPI.JSONChapterName: "Chapter 12"],
                    [MangaSiteAPI.JSONChapterID: "212609", "failure_name": "11", MangaSiteAPI.JSONChapterName: "Chapter 11"]]]
    
                var manga = try! Manga(object: obj)
                expect{ try manga.addChapters(chapters) }.to(throwError())
            }
        }
        
        context("equality") {
            
            it("should display object1 and object2 as equal even with different names") {
                let obj1 = try! Manga(object: [MangaSiteAPI.JSONMangaID: "1234", MangaSiteAPI.JSONMangaName: "TestObject1"])
                let obj2 = try! Manga(object: [MangaSiteAPI.JSONMangaID: "1234", MangaSiteAPI.JSONMangaName: "TestObject2"])
                    
                expect(obj1) == obj2
            }
            
            it("should display object1 and object3 as different, even with same names") {
                let obj1 = try! Manga(object: [MangaSiteAPI.JSONMangaID: "1234", MangaSiteAPI.JSONMangaName: "TestObject1"])
                let obj2 = try! Manga(object: [MangaSiteAPI.JSONMangaID: "1235", MangaSiteAPI.JSONMangaName: "TestObject1"])
                    
                expect(obj1) != obj2
            }
        }
        
        context("an api result json provided") {
            it("should return an array of Mangas") {
                let result = try! [Manga](jsonMangaDict: MangaSiteDemoData.mangaList)
                expect(result.count) == 2
            }
        }
        
        context("merging two Manga Objects") {
            it("should return a correctly merged object") {
                let obj1 = [MangaSiteAPI.JSONMangaID: "3", MangaSiteAPI.JSONMangaName: "\"Aoi\" - Hikaru ga Chikyuu ni Itakoro......"]
                var manga1 = try! Manga(object: obj1)
                var manga2 = try! Manga(object: MangaSiteDemoData.singleManga)
                try! manga2.addChapters(MangaSiteDemoData.mangaChapters)
                
                let result = manga1.merge(manga2)
                
                expect(result.coverImage!) == manga2.coverImage!
                expect(result.author!) == manga2.author!
                expect(result.description!) == manga2.description!
                expect(result.like!) == manga2.like!
                expect(result.favorite!) == manga2.favorite!
                expect(result.status!) == manga2.status!
                expect(result.chapters!.count) == manga2.chapters!.count
            }
            
            it("should return the original object if the objects are not mergable") {
                let obj1 = [MangaSiteAPI.JSONMangaID: "198963", MangaSiteAPI.JSONMangaName: "Test12"]
                let obj2 = [MangaSiteAPI.JSONMangaID: "19", MangaSiteAPI.JSONMangaName: "Test12"]
                
                var manga1 = try! Manga(object: obj1)
                let manga2 = try! Manga(object: obj2)
                
                expect(manga1.merge(manga2)) == manga1
            }
        }
    }
}