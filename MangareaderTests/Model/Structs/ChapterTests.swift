//
//  ChapterTests.swift
//  Mangareader
//
//  Created by Jan Olbrich on 02.01.16.
//  Copyright © 2016 Jan Olbrich. All rights reserved.
//

import Quick
import Nimble

@testable import Mangareader

class ChapterTests: QuickSpec {
    override func spec() {
        context("provided a valid JSON string") {
            it("should return a valid object") {
                let obj = [MangaSiteAPI.JSONChapterID: "212613", MangaSiteAPI.JSONChapterNumber: "12", MangaSiteAPI.JSONChapterName: "Chapter 12"]
                expect{ try Chapter(object: obj) }.toNot(throwError())
            }
            
            it("should contain the correct values") {
                let obj = [MangaSiteAPI.JSONChapterID: "212613", MangaSiteAPI.JSONChapterNumber: "12", MangaSiteAPI.JSONChapterName: "Chapter 12"]
                let chapter = try! Chapter(object: obj)
                    
                expect(chapter.id) == "212613"
                expect(chapter.chapterNumber) == "12"
                expect(chapter.name) == "Chapter 12"
            }
            
            it("should add pages") {
                let obj = [MangaSiteAPI.JSONChapterID: "212613", MangaSiteAPI.JSONChapterNumber: "12", MangaSiteAPI.JSONChapterName: "Chapter 12"]
                let pages = [MangaSiteAPI.JSONPageRoot: [[MangaSiteAPI.JSONPageURL: "google.com"], [MangaSiteAPI.JSONPageURL: "heise.de"], [MangaSiteAPI.JSONPageURL: "golem.de"]]]
                    
                var chapter = try! Chapter(object: obj)
                    
                try! chapter.addPages(pages)
                expect(chapter.pages?.count) == 3
            }
            
            context("an api result json provided") {
                it("should return an array of Chapters") {
                    let result = try! [Chapter](jsonChapterDict: MangaSiteDemoData.mangaChapters)
                    expect(result.count) == 2
                }
            }
        }
    }
}