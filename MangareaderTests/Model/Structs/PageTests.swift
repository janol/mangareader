//
//  PageTest.swift
//  Mangareader
//
//  Created by Jan Olbrich on 04.01.16.
//  Copyright © 2016 Jan Olbrich. All rights reserved.
//

import Quick
import Nimble

@testable import Mangareader

class PageTests: QuickSpec {
    override func spec() {
        context("provided a valid JSON string") {
            it("should return a valid object") {
                let obj = [MangaSiteAPI.JSONPageURL: "google.com"]
                expect{ try Page(object: obj) }.toNot(throwError())
            }
            
            it("should convert to the correct string") {
                let obj = [MangaSiteAPI.JSONPageURL: "manga/test.jpg"]
                let page = try! Page(object: obj)
                expect(page.url.absoluteString).to(equal(MangaSiteAPI.PagesBaseURL + "manga/test.jpg"))
            }
        }
        
        context("provided a broken JSON string") {
            it("should throw an exception") {
                let obj = ["bla": "google.com"]
                expect{ try Page(object: obj) }.to(throwError())
            }
        }
        
        context("an api result json provided") {
            it("should return an array of Pages") {
                let result = try! [Page](jsonPageDict: MangaSiteDemoData.mangaPages)
                expect(result.count) == 2
            }
        }
    }
}