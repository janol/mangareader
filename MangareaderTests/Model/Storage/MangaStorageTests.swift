//
//  MangaStorageTests.swift
//  Mangareader
//
//  Created by Jan Olbrich on 04.01.16.
//  Copyright © 2016 Jan Olbrich. All rights reserved.
//

import Quick
import Nimble
import OHHTTPStubs
import JSONCodable

@testable import Mangareader

class MangaStorageTests: QuickSpec {
    override func spec() {
        var sut: MangaStorage!

        beforeEach { () -> () in
            sut = MangaStorage()
            
            OHHTTPStubs.stubMangaList(MangaSiteDemoData.mangaList)
            OHHTTPStubs.stubMangaChapters(MangaSiteDemoData.mangaChapters)
            OHHTTPStubs.stubMangaDetails(MangaSiteDemoData.mangaDetail)
            OHHTTPStubs.stubMangaPages(MangaSiteDemoData.mangaPages)
        };
        
        afterEach { () -> () in
            OHHTTPStubs.removeAllStubs()
        }
        
        context("the AllMangaList") {
            it("should not be empty") {
                var value = [Manga]()
                sut.allMangaSignal().startWithNext({ (mangas) -> () in
                    value = mangas
                })
                
                expect(value.count).toEventually(beGreaterThan(0))
            }
            
            it("should contain the correct mangas") {
                var value: [Manga]!
                sut.allMangaSignal().startWithNext({ (mangas) -> () in
                    value = mangas
                })
                
                var result: [Manga]!
                do {
                    result = try [Manga](JSONArray: MangaSiteDemoData.mangaList[MangaSiteAPI.JSONMangaRoot]!)
                }
                catch {
                    expect(false)
                }
                
                expect(value).toEventually(equal(result))
            }
        }
        
        context("the chapterForManga call") {
            it("should return a manga with chapters") {
                var manga = try! Manga(object: MangaSiteDemoData.singleManga)
                manga.chapters = []
                
                sut.mangaChaptersSignal(manga).startWithNext({ (resultManga) -> () in
                    manga = resultManga
                })
                
                expect(manga.chapters?.count).toEventually(equal(2))
            }
        }
        
        context("the detailsForManga call") {
            it("should return a manga with more details") {
                var manga = try! Manga(object: MangaSiteDemoData.minimalManga)
                manga.description = ""
                
                sut.mangaDetailsSignal(manga).startWithNext({ (resultManga) -> () in
                    manga = resultManga
                })
                
                expect(manga.description).toNotEventually(equal(""))
            }
        }
        
        context("the chapterPages call") {
            it("should return a chapter with linkes to pages") {
                var chapter = try! Chapter(object: MangaSiteDemoData.singleChapter)
                chapter.pages = []
                
                sut.chapterPagesSignal(chapter).startWithNext { returnChapter in
                    chapter = returnChapter
                }
                
                expect(chapter.pages?.count).toEventually(equal(2))
            }
        }
    }
}