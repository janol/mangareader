//
//  MangaSiteDemoData.swift
//  Mangareader
//
//  Created by Jan Olbrich on 06.01.16.
//  Copyright © 2016 Jan Olbrich. All rights reserved.
//

import Foundation

struct MangaSiteDemoData {
    
    // Mark: -Manga
    
    static let minimalManga = ["id": "3", "name": "\"Aoi\" - Hikaru ga Chikyuu ni Itakoro......"]
    
    static let singleManga = ["id":"3","name":"\"Aoi\" - Hikaru ga Chikyuu ni Itakoro......","img":"cover/Aoi-Hikaru-ga-Chikyuu-ni-Itakoro.jpg","author":"NOMURA Mizuki, YAMASAKI Fuua, CHUN","description":"The story starts with the death of Hikaru Mikado. That guy was a natural charmer,[...]","bolum_sayisi":"16","likeCheck":"false","favCheck":"true","begeni_sayisi":4, "status":"1"]
    
    // Mark: -Chapter
    
    static let singleChapter = ["id":"262406","chapter_number":"15","name":"At That Time, I Was on Earth Hoping to Meet You"]
    
    // Mark: -Page
    
    static let singlePage = ["url":"manga/3/15/p_00001.jpg"]
    
    
    // Mark: -API
    
    static let mangaList = ["manga":[["id":"2","name":"\"Aishiteru\", Uso Dakedo.","img":"cover/Aishiteru-Uso-Dakedo.jpg","author":"Mitsuki Miko","status":"1"],["id":"3","name":"\"Aoi\" - Hikaru ga Chikyuu ni Itakoro......","img":"cover/Aoi-Hikaru-ga-Chikyuu-ni-Itakoro.jpg","author":"NOMURA Mizuki, YAMASAKI Fuua, CHUN","status":"1"]]]
    
    static let mangaDetail = ["manga":[["id":"3","name":"\"Aoi\" - Hikaru ga Chikyuu ni Itakoro......","img":"cover/Aoi-Hikaru-ga-Chikyuu-ni-Itakoro.jpg","author":"NOMURA Mizuki, YAMASAKI Fuua, CHUN","description":"The story starts with the death of Hikaru Mikado. That guy was a natural charmer,[...]","bolum_sayisi":"16","likeCheck":"false","favCheck":"false","begeni_sayisi":4, "status":"1"]]]

    static let mangaChapters = ["chapters":[["id":"262406","chapter_number":"15","name":"At That Time, I Was on Earth Hoping to Meet You"],["id":"262146","chapter_number":"14","name":"The Stars That Fell From the Sky"]]]

    static let clientFavorites = ["manga":[["id":"13889","name":"There's a Reason I'm Undead","img":"cover/there_s_a_reason_i_m_undead.jpg","author":"Hücseyin TUNÇ","status":"2"]]]
    
    static let mangaPages = ["pages":[["url":"manga/3/15/p_00001.jpg"],["url":"manga/3/15/p_00002.jpg"]]]
}