//
//  MockUITableView.swift
//  Mangareader
//
//  Created by Jan Olbrich on 15.01.16.
//  Copyright © 2016 Jan Olbrich. All rights reserved.
//

import Foundation
import UIKit

class MockUITableViewController: UITableViewController {
    let data = [""]
    
    func registerCell(nib: String, reuseIdentifier: String) {
        tableView.registerNib(UINib(nibName: nib, bundle: nil), forCellReuseIdentifier: reuseIdentifier)
    }
    
    override func numberOfSectionsInTableView(tableView: UITableView) -> Int {
        return 1
    }
    
    override func tableView(tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return data.count
    }
}