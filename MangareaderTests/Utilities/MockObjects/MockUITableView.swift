//
//  MockUITableView.swift
//  Mangareader
//
//  Created by Jan Olbrich on 15.01.16.
//  Copyright © 2016 Jan Olbrich. All rights reserved.
//

import Foundation
import UIKit


class MockUITableView: UITableView {
    var didCallReloadData = false
    
    override func reloadData() {
        didCallReloadData = true
    }
}