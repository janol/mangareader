//
//  OHHTTPStubs+MangaSite.swift
//  Mangareader
//
//  Created by Jan Olbrich on 06.01.16.
//  Copyright © 2016 Jan Olbrich. All rights reserved.
//

import Foundation
import UIKit
import OHHTTPStubs

@testable import Mangareader

extension OHHTTPStubs {
    
    static func stubHost(host: String, path: String, json: Dictionary<String, AnyObject>) {
        stub(isHost(host) && isPath(path)) { _ in
            return OHHTTPStubsResponse(JSONObject: json, statusCode: 200, headers: ["ContentType": "application/json"])
        }
    }
    
    static func stubMangaList(mangaJSON: Dictionary<String, AnyObject>) {
        let mangaListURL = NSURL(string: MangaSiteAPI.MangaListURL)!
        OHHTTPStubs.stubHost(mangaListURL.host!, path: mangaListURL.path!, json: mangaJSON)
    }
    
    static func stubMangaDetails(detailJSON: Dictionary<String, AnyObject>) {
        let mangaDetailsURL = NSURL(string: MangaSiteAPI.MangaDetailsURL)!
        OHHTTPStubs.stubHost(mangaDetailsURL.host!, path: mangaDetailsURL.path!, json: detailJSON)
    }
    
    static func stubMangaChapters(chapterJSON: Dictionary<String, AnyObject>) {
        let chaptersURL = NSURL(string: MangaSiteAPI.MangaChapterListURL)!
        OHHTTPStubs.stubHost(chaptersURL.host!, path: chaptersURL.path!, json: chapterJSON)
    }
    
    static func stubUserFavorites(favoriteJSON: Dictionary<String, AnyObject>) {
        let favoritesURL = NSURL(string: MangaSiteAPI.UserFavoritesURL)!
        OHHTTPStubs.stubHost(favoritesURL.host!, path: favoritesURL.path!, json: favoriteJSON)
    }
    
    static func stubMangaPages(pagesJSON: Dictionary<String, AnyObject>) {
        let chapterPagesURL = NSURL(string: MangaSiteAPI.ChapterPagesListURL)!
        OHHTTPStubs.stubHost(chapterPagesURL.host!, path: chapterPagesURL.path!, json: pagesJSON)
    }
    
    static func stubMangaCover(coverImage: UIImage) {
        let coverURL = NSURL(string: MangaSiteAPI.CoverBaseURL)!
        stub(isHost(coverURL.host!) && isPath(coverURL.path!)) { _ in
            return OHHTTPStubsResponse(data: UIImagePNGRepresentation(coverImage)!, statusCode:200, headers:nil)
        }
    }
}